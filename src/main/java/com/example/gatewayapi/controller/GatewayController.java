package com.example.gatewayapi.controller;

import com.google.gson.Gson;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;

@RestController
@RequestMapping("api_fe")
public class GatewayController {
    private static Logger logger = LoggerFactory.getLogger(GatewayController.class);

    @Value("${uri.employee.api}")
    private String uriEmployeeApi;

    @Value("${uri.transaksi.api}")
    private String uriTransaksi;

    //still has bugs
    @GetMapping("/list_employee")
    public ResponseEntity<HashMap<String,Object>> gatewayEmployeeHierarchyById(@RequestParam Integer employeeId) throws IOException {
        logger.info("========== START gatewayEmployeeHierarchyById with employeeId:"+employeeId+" ==========");
        String uri = uriEmployeeApi + "?employeeId=" + employeeId;
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);
        JSONObject jsonRes = new JSONObject(result);
        HashMap<String,Object> mapRes = new Gson().fromJson(jsonRes.toString(), HashMap.class);

        logger.info("========== END gatewayEmployeeHierarchyById ==========");
        return new ResponseEntity<HashMap<String,Object>>(mapRes, HttpStatus.OK);
    }

    @GetMapping("/post_data")
    public ResponseEntity<HashMap<String,Object>> gatewayCSVTransaksi(@RequestParam("file") MultipartFile file) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", file);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> result = restTemplate.postForEntity(uriTransaksi, requestEntity, String.class);
        JSONObject jsonRes = new JSONObject(result);
        HashMap<String,Object> mapRes = new Gson().fromJson(jsonRes.toString(), HashMap.class);

        return new ResponseEntity<HashMap<String,Object>>(mapRes, HttpStatus.OK);
    }
}
